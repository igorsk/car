
Technologies:

-Spring Boot

-Spring Data JPA

-Embedded Tomcat

-H2 Database


To run the application:

1) from the project root execute:

 -on Unix:     ./mvnw spring-boot:run

 -on Windows:   mvnw.cmd spring-boot:run

2) open at: http://localhost:8080/

3) to exit application press ctrl-c
