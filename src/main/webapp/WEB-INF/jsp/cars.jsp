<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>      
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>      
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Cars</title>
	</head>
	<body>


		<div>Car numbers:</div>
		<div>
            <table>
                 <c:forEach var="car" items="${cars}">
                      <tr>
                       <td>${car.number}</td>
                      </tr>
                 </c:forEach>

            </table>
        </div>

	 	<form:form action="/" modelAttribute="car" method="post">
			<div style="float:left">
			    <label for="number">Number</label>
   				<input type="text" name="number" id="number" />
   			 	<form:errors path="number" element="div" style="color:red" />
		  	</div>
		  <button type="submit">Add car</button>
		</form:form>
			
	</body>
</html>