package com.registration.car.controller;


import com.registration.car.model.Car;
import com.registration.car.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

@Controller
@RequestMapping
public class CarController {

    private static final String CAR = "car";

    @Autowired
    private CarRepository carRepository;


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String view(HttpServletRequest request, HttpServletResponse response, Model model) {

        Iterable<Car> cars = carRepository.findAll();

        if (!model.containsAttribute(CAR)) {
            model.addAttribute(CAR, new Car());
        }

        model.addAttribute("cars", cars);
        return "cars";
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public String saveData(@Valid @ModelAttribute(CAR) Car car, BindingResult bindingResult, RedirectAttributes redirectAttributes) {

        if (bindingResult.hasErrors()) {
            redirectAttributes.addFlashAttribute("org.springframework.validation.BindingResult." + CAR, bindingResult);
            redirectAttributes.addFlashAttribute(CAR, car);
            return "redirect:/";
        }

        carRepository.save(car);

        return "redirect:/";
    }

}
